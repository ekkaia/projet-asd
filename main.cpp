#include <iostream>
#include <cmath>
#include <cstdlib>
#include <string.h>
#include <chrono>
#include <ctime>


/*
 * OLIVIER Léo
 * COUËDEL Sofiane
 *
 * 03/01/2020
 *
 * Algorithmique et Structure de Données
 */
// Alias pour n'importe quel type
typedef long DATATYPE;

// Chaînage
typedef struct _datum {
    DATATYPE valeur;
    _datum *suiv;
} _data;

typedef _data* p_data;

typedef struct _datalist {
	int capa;
	int nbmono;
	p_data *monotonies;
} datalistes;

using namespace std;

/**
 * @brief Crée un nouveau maillon avec la valeur "uneVal" et retourne la tête d'un chaînage
 * commençant par ce maillon et continuant avec "chain".
 *
 * @param uneVal Valeur à ajouter en tête de "chain".
 * @param chain Chaîne dont la tête va devenir "uneVal"
 * @return p_data Nouvelle chaîne commençant par "uneVal".
 */
p_data ajoutDevant(DATATYPE uneVal, p_data chain) {
    // Nouveau pointeur de tête
    p_data ptrTete;
    ptrTete = new(_data);

    ptrTete->valeur = uneVal;
    ptrTete->suiv = chain;

    return ptrTete;
}

/**
 * @brief Crée et renoie une structure (de type datalistes) initialisée avec un tableau
 * de nb chaînages (non valués).
 *
 * @param nb Nombre de cases du tableau.
 * @return datalistes
 */
datalistes initT(int nb){
	datalistes tab;
	tab.capa = nb;
	tab.nbmono = 0;
	tab.monotonies = new p_data[nb];

	return tab;
}

/**
 * @brief Affiche sur une même ligne les valeurs stockées dans le chaînage "chain".
 *
 * @param chain Chaîne à afficher.
 */
void aff(p_data chain) {
    cout << "Chaine :" << endl;
	if (chain != nullptr) {
		cout << "   Valeur de " << chain << " : " << chain->valeur << endl;
		while (chain->suiv != nullptr) {
			chain = chain->suiv;
			cout << "   Valeur de " << chain << " : " << chain->valeur << endl;
		}
	} else {
		cout << "   Vide !" << endl;
	}
}

/**
 * @brief Affiche (une ligne par case) les valeurs stockées dans le tableau mono
 *
 * @param mono Tableau à afficher.
 */
void affT(datalistes mono) {
	cout << "Datalistes mono :" << endl;
    for (int i = 0 ; i < mono.nbmono ; i++) {
        aff(mono.monotonies[i]);
        cout << " \n" ;
    }
}

/**
 * @brief Lit (sur l'entrée standard) une succession de nb valeurs et les mémorise (dans le même ordre)
 * dans un chaînage terminé par nullptr, dont la tête est renvoyée.
 *
 * @param nb Nombre de valeurs à entrer dans le chaînage.
 * @return p_data Chaînage composé de nb valeurs.
 */
p_data saisieNombre(int nb) {
	cout << "Saisissez " << nb << " valeurs :" << endl;
    p_data ptrSaisie = new(_data);
    p_data ptrTete = ptrSaisie;

    for (int i = 0 ; i < nb ; i++) {
		p_data ptrSuiv = new(_data);
        ptrSaisie->suiv = ptrSuiv;

		if (i == nb - 1) {
			cin >> ptrSaisie->valeur;
			ptrSaisie->suiv = nullptr;
		} else {
        	cin >>  ptrSaisie->valeur;
			ptrSaisie = ptrSaisie->suiv;
		}
    }

	return ptrTete;
}

/**
 * @brief qui lit (sur l’entrée standard) une succession de valeurs et les
 * mémorise (dans le même ordre) dans un chaînage terminé par nullptr, dont la tête est renvoyée.
 *
 * @param sentinelle DATATYPE qui sert de stop de la fonction.
 * @return p_data Chaînage des DATATYPE donné par l'utilisateur.
 */
p_data saisieBorne(DATATYPE sentinelle) {
	p_data tete = new(_data);
	p_data tampon = new(_data);

	tampon = nullptr;
	DATATYPE stop;

	cin >> stop;
	tete->valeur = stop;
	tete->suiv = nullptr;
	tampon = tete;

	while (stop != sentinelle) {
		cin >> stop;
		p_data nouv_maille = new(_data);
		nouv_maille->valeur = stop;
		nouv_maille->suiv = nullptr;
		tampon->suiv = nouv_maille;
		tampon = nouv_maille;
	}

	return tete;
}

/**
 * @brief VERSION ITÉRATIVE : Fusionne deux chaînages entre eux en ordre croissante.
 * PRE : les chaînages sont triés en ordre croissant
 * POST : prem prend toutes les valeurs de sec donc sa composition originale n'est pas préservée.
 *
 * @param prem Chaîne qui sera fusionné avec sec.
 * @param sec Chaine qui sera fusionné avec prem.
 * @return p_data Chaînage fusion de prem et sec en ordre croissant.
 */
p_data fusionIter(p_data prem, p_data sec){
    p_data tampon, ajout;

    ajout = new(_data);
    tampon = new (_data);

    if (prem->valeur < sec->valeur){
        ajout = prem;
        prem = prem->suiv;
    } else {
        ajout = sec;
        sec = sec->suiv;
    }

    tampon = ajout;

    while (prem != nullptr && sec != nullptr){
        if (prem->valeur < sec->valeur){
            ajout->suiv = prem;
            prem = prem->suiv;
        } else {
            ajout->suiv = sec;
            sec = sec->suiv;
        }

        ajout = ajout->suiv;
    }
    if (prem != nullptr){
        ajout->suiv = prem;
    }
    if (sec != nullptr){
        ajout->suiv = sec;
    }

    return tampon;
}

/**
 * @brief VERSION RÉCURSIVE : Fusionne deux chaines entre elles en ordre croissante.
 * PRE : les chaînages sont triés en ordre croissant
 * POST : prem prend toutes les valeurs de sec donc sa composition originale n'est pas préservée.
 * 
 * @param prem 
 * @param sec 
 * @return p_data 
 */
p_data fusionRecur(p_data prem, p_data sec){
    if ( prem == nullptr) {
		return sec;
	} else if ( sec == nullptr) {
		return prem;
	} else if (prem->valeur < sec->valeur) {
		prem->suiv = fusionRecur(prem->suiv, sec);
		return prem;
	} else {
		sec->suiv = fusionRecur(prem, sec->suiv);
		return sec;
	}
}

/**
 * @brief Compte le nombre de monotonies croissantes dans un chaînage terminé par nullptr.
 *
 * @param chain Chaîne sur laquelle compter les monotonies croissantes.
 * @return int Nombre de monotonies croissantes.
 */
int nbCroissances(p_data chain) {
    int nbMonotonies = 1;
    while (chain->suiv != nullptr) {
        if (chain->valeur > chain->suiv->valeur) {
            nbMonotonies++;
        }

        chain = chain->suiv;
    }

	return nbMonotonies;
}

/**
 * @brief Ajoute dans le tableau mono le chaînage chain (dans la prochaine case non utilisée).
 * PRE : chain ne vaut pas nullptr
 * POST : mono sera une case plus long, chain ne sera pas modifié 
 * 
 * @param chain
 * @param mono
 * @return p_data
 */
void ajouterFin(p_data chain, datalistes& mono) {
	mono.monotonies[mono.nbmono] = chain;
	mono.nbmono++;
}

/**
 * @brief Place dans le chaînage "mono" la première monotonie croissante de "chain"
 * et l'en retire.
 * PRE : chain ne vaut pas nullptr
 * POST : chain aura perdu sa première monotonie 
 *
 * @param chain chaîne dont la première monotonie sera extraite et retirée.
 * @param mono chaîne dans laquelle sera placée en tête la première monotonie de "chain".
 */
/*void extraireCroissance(p_data& chain, p_data& mono) {
    p_data teteChain = chain;

    while (chain->suiv != nullptr && chain->valeur <= chain->suiv->valeur) {
        chain = chain->suiv;
    }
    chain->suiv = mono;
    mono = teteChain;

   aff(mono);
   aff(teteChain);
}*/

void extraireCroissance(p_data & chain, p_data & mono) {
    p_data temp;
    mono = ajoutDevant(chain->valeur, nullptr);
    chain = chain->suiv;
    temp = mono;
    while(chain !=nullptr && chain->valeur >= temp->valeur){
        temp->suiv = new(_data);
        temp = temp->suiv;
        temp->valeur = chain->valeur;
        temp->suiv = nullptr;
        chain = chain->suiv;
    }
}


/**
 * @brief Supprime du tableau mono et renvoie le dernier chaînage encore
 * stocké (celui de plus grand indice).
 * PRE : il y a un chaînage à supprimer dans mono (mono non vide)
 * POST : mono sera une case plus court
 *
 * @param mono
 * @return p_data
 */
p_data suppressionFin(datalistes& mono) {
	mono.monotonies[mono.nbmono] = nullptr;
	mono.nbmono--;

	return mono.monotonies[mono.nbmono];
}

/**
 * @brief Renvoie un chaînage formé en mettant bout à bout (dans l'odre)
 * tous les chaînages stockés dans le tableau mono, en les y enlevant.
 * PRE : mono n'est pas vide
 * POST : le tableau mono sera vide
 *
 * @param mono
 * @return p_data
 */
p_data suppressionTotale(datalistes& mono) {
	p_data chainFusion = new(_data);
	p_data queue = new(_data);

	chainFusion = mono.monotonies[0];

	for (int i = 0 ; i < mono.nbmono ; i++) {
		while (mono.monotonies[i]->suiv != nullptr) {
			mono.monotonies[i] = mono.monotonies[i]->suiv;
			queue = mono.monotonies[i];
		}

		queue->suiv = mono.monotonies[i + 1];
		mono.monotonies[i] = nullptr;
	}

	return chainFusion;
}

/**
 * @brief retourne un tableau dont les cases contiennent (dans l’ordre)
 *  les monotonies croissantes du chaînage contenu dans chain.
 *
 * @param chain
 * @return datalistes
 */
datalistes separation(p_data& chain) {
	datalistes tab;
	p_data mono;

	int nb = nbCroissances(chain);
	tab = initT(nb);

	for(int i = 0; i < nb; i++){
		extraireCroissance(chain, mono);
		ajouterFin(mono, tab);
	}

	return tab;
}

/**
 * @brief trie en ordre croissant les monotonies d'un datalistes.
 *
 * @param tabmono datalistes de monotonies.
 */
void trier(datalistes& tabmono){

	p_data debut, fin;
	int nb = tabmono.nbmono - 1;

	for (int i = 0; i < nb; i++) {
		debut = tabmono.monotonies[0];
		fin = suppressionFin(tabmono);
		tabmono.monotonies[0] = fusionIter(debut, fin);
	}
}

/**
 * @brief trie en ordre croissant un chaînage (fusion multiple).
 *
 * @param chain p_data chainage de datatype.
 */
void trier(p_data& chain) {
	datalistes tab;

	tab = separation(chain);
	trier(tab);
	chain = suppressionFin(tab);
}

int main() {
    p_data testTrier = new(_data);
    p_data test2 = new(_data);

    testTrier->valeur = 11;
    testTrier = ajoutDevant(8, testTrier);
    testTrier = ajoutDevant(7, testTrier);
    testTrier = ajoutDevant(9, testTrier);
    testTrier = ajoutDevant(5, testTrier);
    testTrier = ajoutDevant(6, testTrier);
    testTrier = ajoutDevant(4, testTrier);
    testTrier = ajoutDevant(2, testTrier);
    testTrier = ajoutDevant(12, testTrier);
    testTrier = ajoutDevant(15, testTrier);
    testTrier = ajoutDevant(3, testTrier);
    testTrier = ajoutDevant(1, testTrier);

    test2->valeur = 14;
    test2 = ajoutDevant(13, test2);
    test2 = ajoutDevant(9, test2);
    test2 = ajoutDevant(5, test2);
    test2 = ajoutDevant(4, test2);

    cout << "Affichage de liste desordonnée :\n" << endl;
	aff(testTrier);

	trier(testTrier);

    cout << "\nAffichage de liste ordonnée :\n" << endl;
	aff(testTrier);
}